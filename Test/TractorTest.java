import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class TractorTest {
    Tractor test = new Tractor("MTZ", "D-243", 65, 14000, 238);
    Tractor test1 = new Tractor.Builder()
            .setBrand("Asd-12")
            .setTypeOfEngine("SMD-18")
            .setPower(80)
            .setTractionClass(5000)
            .setFuel(210)
            .build();
    @Test
    public void brandTest()
    {
        //Tractor test = new Tractor();
        test.setBrand("MTZ");
        assertEquals(test.getBrand(), "MTZ");
    }
    @Test
    public void typeOfEngineTest()
    {
        //Tractor test = new Tractor();
        test.setTypeOfEngine("D-243");
        assertEquals(test.getTypeOfEngine(), "D-243");
    }
    @Test
    public void powerTest()
    {
        //Tractor test = new Tractor();
        test.setPower(65);
        assertEquals(test.getPower(), 65);
    }
    @Test
    public void tractionClassTest()
    {
        //Tractor test = new Tractor();
        test.setTractionClass(14000);
        assertEquals(test.getTractionClass(), 14000);
    }
    @Test
    public void fuelTest()
    {
        //Tractor test = new Tractor();
        test.setFuel(238);
        assertEquals(test.getFuel(), 238);
    }
    @Test
    public void toStringTest()
    {
        assertEquals(test.toString(),"Brand = MTZ Type_of_engine = D-243 Power = 65 Traction_Class = 14000 Fuel = 238");
    }
    @Test
    public void equalsTest()
    {
        boolean rez = test.equals(new Tractor("MTZ", "D-243", 65, 14000, 238));
        assertEquals(rez, true);
    }
    /**
     * Testing validation
     * */
    @Test
    public void setBuildBrandTest()
    {
       // Tractor test = new Tractor.Builder().setBrand("Asd12").build();
       // assertEquals(test.getBrand(), "Asd12");
        //Tractor test1 = new Tractor.Builder().setBrand("Asd-12").build();
        assertEquals(test1.getBrand(), "Asd-12");
    }
    @Test
    public void setBuildTypeOfEngineTest()
    {
        //Tractor test = new Tractor.Builder().setTypeOfEngine("SMD-18").build();
        assertEquals(test1.getTypeOfEngine(), "SMD-18");

    }
    @Test
    public void setBuildPowerTest()
    {
        assertEquals(test1.getPower(), 80);
    }
    @Test
    public void setBuilderTractionClassTest()
    {
        assertEquals(test1.getTractionClass(), 5000);
    }
    @Test
    public void setBuilderFuelTest()
    {
        assertEquals(test1.getFuel(), 210);
    }
}