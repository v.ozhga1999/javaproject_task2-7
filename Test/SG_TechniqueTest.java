import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class SG_TechniqueTest {

        SG_Technique test = new SG_Technique(5000, 2000, 2400, 6240, 700);
        SG_Technique test1 = new SG_Technique.Builder()
            .setMassa(1234)
            .setWidth(123)
            .setHeight(123)
            .setLenght(3400)
            .setRoadClearance(1223)
            .build();
        @Test
        public void getMassaTest()
        {
            //SG_Technique test = new SG_Technique();
            test.setMassa(5000);
            assertEquals(test.getMassa(), 5000);
        }
        @Test
        public void getWidthTest()
        {
            //SG_Technique test = new SG_Technique();
            test.setWidth(2000);
            assertEquals(test.getWidth(), 2000);
        }
        @Test
        public void getHeightTest()
        {
            //SG_Technique test = new SG_Technique();
            test.setHeight(2400);
            assertEquals(test.getHeight(), 2400);
        }
        @Test
        public void getLenghtTest()
        {
            //SG_Technique test = new SG_Technique();
            test.setLenght(6240);
            assertEquals(test.getLenght(), 6240);
        }
        @Test
        public void roadClearance()
        {
            //SG_Technique test = new SG_Technique();
            test.setRoadClearance(700);
            assertEquals(test.getRoadClearance(), 700);
        }
        @Test
        public void toStringTest()
        {

            assertEquals(test.toString(), "massa = 5000 width = 2000 height = 2400 lenght = 6240 road_Clearance = 700");
        }
        @Test
        public void equalsTest()
        {
            boolean rez = test.equals(new SG_Technique(5000, 2000, 2400, 6240, 700));
            assertEquals(rez, true);
        }
        /**
        * Testing validation
        * */
        @Test
        public void setBuildMassaTest()
        {
            assertEquals(test1.getMassa(), 1234);
        }
        @Test
        public void setBuildWidthTest()
        {
            assertEquals(test1.getWidth(), 123);
        }
        @Test
        public void setBuildHeightTest()
        {
            assertEquals(test1.getHeight(), 123);
        }
        @Test
        public void setBuildLenghtTest()
        {
            assertEquals(test1.getLenght(), 3400);
        }
        @Test
        public void setBuildRoadClearancetest()
        {
            assertEquals(test1.getRoadClearance(), 1223);
        }
}

