import javax.xml.bind.JAXBException;
import java.io.*;

public class TxtSerialize implements Serializer {
    @Override
    public void serialize(Tractor tractor) {
        File myFile = new File("myText.txt");

        try{
            FileWriter writer = new FileWriter(myFile);
            writer.write(String.valueOf(tractor));
            writer.close();
        }catch(IOException ex){
            ex.printStackTrace();
        }
    }

    @Override
    public Tractor deserialize() {
        Tractor tractorTxt = null;
        try{
            File myFile = new File("myText.txt");
            FileReader reader = new FileReader(myFile);
            BufferedReader br = new BufferedReader(reader);
            String str = br.readLine();
            String[] obj = new String[10];
            int k  = 0;
            for (int i = 0; i < str.length(); i++) {

                if (str.charAt(i) == '=') {
                    obj[k] = "";
                    for (int j = i + 2;(j < str.length()) && (str.charAt(j) != ' '); j++) {
                        obj[k] += str.charAt(j);
                    }
                    k++;
                }
            }
            //for(int i = 0; i < 5; i++)
            //{
            //    System.out.println(obj[i] + "\n");
            //}
            tractorTxt = new Tractor.Builder()
                    .setBrand(obj[0])
                    .setTypeOfEngine(obj[1])
                    .setPower(Integer.parseInt(obj[2]))
                    .setTractionClass(Integer.parseInt(obj[3])+1)
                    .setFuel(Integer.parseInt(obj[4]))
                    .build();
            //System.out.println(str);
            //System.out.println(tractorTxt.toString());
            br.close();
            reader.close();
        }catch(IOException ex){
            ex.printStackTrace();
        }
        return tractorTxt;
    }
}
