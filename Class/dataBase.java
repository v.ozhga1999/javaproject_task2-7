import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class dataBase {
    private String url = new String("jdbc:postgresql://localhost/base_for_java");
    private String user = new String("postgres");
    private String password = new String("postgres");
    public void writeDataBaseTractor(Collection<Tractor> tractorCollection)
    {
        try
        {
            Connection db = DriverManager.getConnection(url, user, password);
            Statement statement = db.createStatement();
            ResultSet rest = statement.executeQuery("select * from tractor;");
            while(rest.next())
            {
                List<Tractor> tractorList = new ArrayList<>();
                String brand = new String(rest.getString("t_brand"));
                String typeOfEngine = rest.getString("t_type_of_engine");
                int power = rest.getInt("t_power");
                int tractionClass = rest.getInt("t_traction_class");
                int fuel = rest.getInt("t_fuel");
                Tractor tractor = new Tractor.Builder()
                        .setBrand(brand)
                        .setTypeOfEngine(typeOfEngine)
                        .setPower(power)
                        .setTractionClass(tractionClass)
                        .setFuel(fuel)
                        .build();
                tractorCollection.add(tractor);
            }
            //System.out.println("DataBase" + tractorCollection);
            Iterator<Tractor> iterator = tractorCollection.iterator();
            while (iterator.hasNext()) {
                System.out.println(iterator.next());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void writeDataBaseTractorAndCharacteristics(Collection<Tractor> tractorCollection)
    {
        try
        {
            Connection db = DriverManager.getConnection(url, user, password);
            Statement statement = db.createStatement();
            ResultSet rest = statement.executeQuery("select * from tractor t, sg_technique s where t.t_id = s.s_id;");
            while(rest.next())
            {
                List<Tractor> tractorList = new ArrayList<>();
                //First table
                String brand = new String(rest.getString("t_brand"));
                String typeOfEngine = rest.getString("t_type_of_engine");
                int power = rest.getInt("t_power");
                int tractionClass = rest.getInt("t_traction_class");
                int fuel = rest.getInt("t_fuel");
                //Second table
                int massa = rest.getInt("s_massa");
                int width = rest.getInt("s_width");
                int height = rest.getInt("s_height");
                int lenght = rest.getInt("s_lenght");
                int roadClearanse = rest.getInt("s_road_clearence");
                Tractor tractor = new Tractor.Builder()
                        .setBrand(brand)
                        .setTypeOfEngine(typeOfEngine)
                        .setPower(power)
                        .setTractionClass(tractionClass)
                        .setFuel(fuel)
                        .setMassa(massa)
                        .setWidth(width)
                        .setHeight(height)
                        .setLenght(lenght)
                        .setRoadClearance(roadClearanse)
                        .build();
                tractorCollection.add(tractor);
            }
            //System.out.println("DataBase" + tractorCollection);
            Iterator<Tractor> iterator = tractorCollection.iterator();
            while (iterator.hasNext()) {
                System.out.println(iterator.next());
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    public void writeDataBaseTractorFuelConsumption(Collection<Tractor> tractorCollection)
    {
        try
        {
            Connection db = DriverManager.getConnection(url, user, password);
            Statement statement = db.createStatement();
            ResultSet rest = statement.executeQuery("select t_brand, t_type_of_engine, t_traction_class, s_massa, (t_power*t_fuel/1000) \n" +
                    "from tractor t, sg_technique s \n" +
                    "where t.t_id = s.s_id;");
            while(rest.next())
            {
                List<Tractor> tractorList = new ArrayList<>();
                //First table
                String brand = new String(rest.getString("t_brand"));
                String typeOfEngine = rest.getString("t_type_of_engine");
                int tractionClass = rest.getInt("t_traction_class");
                int fuel = rest.getInt("?column?");
                //Second table
                int massa = rest.getInt("s_massa");
                Tractor tractor = new Tractor.Builder()
                        .setBrand(brand)
                        .setTypeOfEngine(typeOfEngine)
                        .setTractionClass(tractionClass)
                        .setMassa(massa)
                        .setFuel(fuel)
                        .build();
                tractorCollection.add(tractor);
            }
            //System.out.println("DataBase" + tractorCollection);
            Iterator<Tractor> iterator = tractorCollection.iterator();
            while (iterator.hasNext()) {
                System.out.println(iterator.next());
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }
}
