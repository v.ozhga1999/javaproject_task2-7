import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.sun.xml.internal.ws.developer.SerializationFeature;
import com.sun.xml.internal.ws.wsdl.writer.document.Service;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;

public class JsonSerialize implements Serializer
{
    private String json = null;
    @Override
    public void serialize(Tractor tractor) throws IOException, JAXBException {
        Gson gson = new Gson();
        json = gson.toJson(tractor);
        System.out.println(json);
    }

    @Override
    public Tractor deserialize() throws IOException, JAXBException {
        Gson gson = new Gson();
        Tractor out = gson.fromJson(json, Tractor.class);
        return out;
    }
}
