import javax.xml.bind.JAXBException;
import java.io.IOException;

public interface Serializer {
    Tractor deserialize() throws IOException, JAXBException;

    void serialize(Tractor tractor) throws IOException, JAXBException;
}
