//import com.sun.istack.internal.NotNull;
//import javax.validation.
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class SG_Technique {
    @NotNull(message="Massa cannot by null")
    protected int massa;
    @NotNull(message="Width cannot by null")
    protected int width;
    @NotNull(message="Height cannot by null")
    protected int height;
    @NotNull(message="Lenght cannot by null")
    protected int lenght;
    @Pattern(regexp ="^[0-9]{1,4}$",message = "RoadClearance cannot by null")
    protected int roadClearance;
    /*---------------------------------------------------------------------*/
    public void setMassa(int massa)
    {
        this.massa = massa;
    }
    public void setWidth(int width)
    {
        this.width = width;
    }
    public void setHeight(int height)
    {
        this.height = height;
    }
    public void setLenght(int lenght)
    {
        this.lenght = lenght;
    }
    public void setRoadClearance(int roadClearance)
    {
        this.roadClearance = roadClearance;
    }
        //================================================
    public int getMassa ()
    {
        return massa;
    }
    public int getWidth ()
    {
        return width;
    }
    public int getHeight ()
    {
        return height;
    }
    public int getLenght ()
    {
        return lenght;
    }
    public int getRoadClearance ()
    {
        return roadClearance;
    }
        /*-------------------------------------------------------------------*/
    protected SG_Technique(int massa, int width, int height, int lenght, int roadClearance)
    {
        this.massa = massa;
        this.width = width;
        this.height = height;
        this.lenght = lenght;
        this.roadClearance = roadClearance;
    }
    protected SG_Technique(){}

    protected final static int checkMassaMin = 1, checkMassaMax = 10000;
    protected final static int checkWidthMin = 1, checkWidthMax = 2400;
    protected final static int checkHeightMin = 1, checkHeightMax = 3400;
    protected final static int checkLenghtMin = 1, checkLenghtMax = 12000;
    protected final static int checkRoadClearanceMin = 250, checkRoadClearanceMax = 2720;
    //============Builder==============================================================
    public static class Builder
    {
        private SG_Technique sgTechniqueToBuild;
        public Builder()
        {
            sgTechniqueToBuild = new SG_Technique();
        }
        SG_Technique build()
        {
            SG_Technique  buildSgTechnique = sgTechniqueToBuild;
            sgTechniqueToBuild = new SG_Technique();
            return buildSgTechnique;
        }
        public Builder setMassa(int massa)
        {
            if(massa >= SG_Technique.checkMassaMin && massa <= SG_Technique.checkMassaMax)
                this.sgTechniqueToBuild.massa = massa;
                else
                    throw new IllegalArgumentException("The massa must be higher then 1 and lower then 10000");
            return this;
        }
        public Builder setWidth(int width)
        {
            if(width >= SG_Technique.checkWidthMin && width <= SG_Technique.checkWidthMax)
                this.sgTechniqueToBuild.width = width;
                else
                    throw new IllegalArgumentException("The widht must be higher then 1 and lower then 2400");
            return this;
        }
        public Builder setHeight(int height)
        {
            if(height >= SG_Technique.checkHeightMin && height <= SG_Technique.checkHeightMax)
                this.sgTechniqueToBuild.height = height;
                else
                    throw new IllegalArgumentException("The height must be higher then 1 and lower then 2400");
            return this;
        }
        public Builder setLenght(int lenght)
        {
            if(lenght >= SG_Technique.checkLenghtMin && lenght <= SG_Technique.checkLenghtMax)
                this.sgTechniqueToBuild.lenght = lenght;
                else
                    throw new IllegalArgumentException("The lenght must be higher then 1 and lower then 12000");
            return this;
        }
        public Builder setRoadClearance(int roadClearance)
        {
            if(roadClearance >= SG_Technique.checkRoadClearanceMin && roadClearance <= SG_Technique.checkRoadClearanceMax)
                this.sgTechniqueToBuild.roadClearance = roadClearance;
                else
                    throw new IllegalArgumentException("The RoadClearance must be higher then 1 and lower then 2720");
            return this;
        }
    }
    //==================================================================

    @Override
    public String toString() {
        String str;
        str = "massa = " + this.massa + " width = " + this.width + " height = " + this.height + " lenght = " + this.lenght +
                " road_Clearance = " + this.roadClearance;
        return str;
    }
    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) return true;
        if((obj == null) || (getClass() != obj.getClass())) return false;
        SG_Technique other = (SG_Technique) obj;
        if(massa != other.massa || width != other.width || height != other.height || lenght != other.lenght
                || roadClearance != other.roadClearance)
            return false;
        return true;
    }
    @Override
    public int hashCode()
    {
        final int mult = 20;
        int result = 1;
        result = mult * result + massa;
        result = mult * result + width;
        result = mult * result + height;
        result = mult * result + lenght;
        result = mult * result + roadClearance;
        return result;
    }

}

