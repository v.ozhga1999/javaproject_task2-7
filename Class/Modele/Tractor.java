//import javax.validation.constraints.NotNull;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
//import java.util.regex.Pattern;
import java.util.List;
import java.util.regex.Matcher;
@XmlRootElement
public class Tractor extends SG_Technique implements Comparable<Tractor> {
    @NotNull(message = "Brand cannot by null")
    private String brand;
    @NotNull(message = "TypeOfEngine by null")
    private String typeOfEngine;
    @Size(min = 1, max = 350, message = "Power must be between 0 and 350")
    private int power;
    @Min(value = 10000, message = "TractionClass should not be less than 4000")
    private int tractionClass;
    @Pattern(regexp = "^[0-9]{3}$", message = "Fuel cannot by null")
    private int fuel;

    /*----------------------------------------------------------*/
    @XmlElement
    public void setBrand (String brand)
    {
        this.brand = brand;
    }
    @XmlElement
    public void setTypeOfEngine (String typeOfEngine)
    {
        this.typeOfEngine = typeOfEngine;
    }
    @XmlElement
    public void setPower (int power)
    {
        this.power = power;
    }
    @XmlElement
    public void setTractionClass (int tractionClass)
    {
        this.tractionClass = tractionClass;
    }
    @XmlElement
    public void setFuel (int fuel)
    {
        this.fuel = fuel;
    }
    //============================================

    /**
     * @return brand
     */
    public String getBrand ()
    {
        return brand;
    }
    /**
     * @return typeOfEngine
    */
    public String getTypeOfEngine ()
    {
        return typeOfEngine;
    }
    /**
     * @return power
     */
    public int getPower ()
    {
        return power;
    }
    /**
     * @return getTractionClass
     */
    public int getTractionClass ()
    {
        return tractionClass;
    }
    /**
     * @return fuel
     */
    public int getFuel ()
    {
        return fuel;
    }
    /*------------------------------------------------------------------------*/

    public Tractor(String brand, String typeOfEngine, int power, int tractionClass, int fuel)
    {
        this.brand = brand;
        this.typeOfEngine = typeOfEngine;
        this.power = power;
        this.tractionClass = tractionClass;
        this.fuel = fuel;
    }
    private Tractor(){}
    //================Builder=================================
    private final static String patternBrand = "^[a-zA-Z]{1,8}(-[0-9]{1,6})?$";
    private final static String patternTypeOfEngine = "^[a-zA-Z]{1,5}((\\ |-)[0-9]{1,6})?$";
    private final static int checkPowerMin = 1, checkPowerMax = 350;
    private final static int checkTractionClassMin = 1, checkTractionClassMax = 50000;
    private final static int checkFuelMin = 1, checkFuelMax = 238;

    public static class Builder extends SG_Technique.Builder
    {
        private Tractor tractorToBuild;
        public Builder()
        {
            tractorToBuild = new Tractor();
        }
        Tractor build()
        {
            Tractor buildTractor = tractorToBuild;
            tractorToBuild = new Tractor();
            return buildTractor;
        }
        public Builder setBrand(String brand)
        {
            //java.util.regex.Pattern p = java.util.regex.Pattern.compile(patternBrand);
            //Matcher m = p.matcher(brand);
            //if(m.matches())
            if(java.util.regex.Pattern.matches(patternBrand, brand))
                this.tractorToBuild.brand = brand;
                else
                    throw new IllegalArgumentException("Value brand must match with 'Example-123' or 'example-1234' or 'EXAMPLE-123'");
            return this;
        }
        public Builder setTypeOfEngine(String typeOfEngine)
        {
            if(java.util.regex.Pattern.matches(patternTypeOfEngine, typeOfEngine))
                this.tractorToBuild.typeOfEngine = typeOfEngine;
                else
                    throw new IllegalArgumentException("Enter value typeOfEngine which matches 'sdf' or 'sdf 876' or 'Sdf-123' or 'WW-123'");
            return this;
        }
        public Builder setPower(int power)
        {
            if(power >= Tractor.checkPowerMin && power <= Tractor.checkPowerMax)
                this.tractorToBuild.power = power;
                else
                    throw new IllegalArgumentException("The power must be higher then 1 and lower then 350");
            return this;
        }
        public Builder setTractionClass(int tractionClass)
        {
            if(tractionClass >= Tractor.checkTractionClassMin && tractionClass <= Tractor.checkTractionClassMax)
                this.tractorToBuild.tractionClass = tractionClass;
                else
                    throw new IllegalArgumentException("The tractionClass must be higher then 1 and lower then 10000");
            return this;
        }
        public Builder setFuel(int fuel)
        {
            if(fuel >= Tractor.checkFuelMin && fuel <= Tractor.checkFuelMax)
                this.tractorToBuild.fuel = fuel;
                else
                    throw new IllegalArgumentException("The fuel must be higher then 1 and lower then 238");
            return this;
        }
        //Builder wiht SG_Technique==================================================
        //====================================================================
        //====================================================================
        public Builder setMassa(int massa)
        {
            if(massa >= SG_Technique.checkMassaMin && massa <= SG_Technique.checkMassaMax)
                this.tractorToBuild.massa = massa;
            else
                throw new IllegalArgumentException("The massa must be higher then 1 and lower then 10000");
            return this;
        }
        public Builder setWidth(int width)
        {
            if(width >= SG_Technique.checkWidthMin && width <= SG_Technique.checkWidthMax)
                this.tractorToBuild.width = width;
            else
                throw new IllegalArgumentException("The widht must be higher then 1 and lower then 2400");
            return this;
        }
        public Builder setHeight(int height)
        {
            if(height >= SG_Technique.checkHeightMin && height <= SG_Technique.checkHeightMax)
                this.tractorToBuild.height = height;
            else
                throw new IllegalArgumentException("The height must be higher then 1 and lower then 2400");
            return this;
        }
        public Builder setLenght(int lenght)
        {
            if(lenght >= SG_Technique.checkLenghtMin && lenght <= SG_Technique.checkLenghtMax)
                this.tractorToBuild.lenght = lenght;
            else
                throw new IllegalArgumentException("The lenght must be higher then 1 and lower then 12000");
            return this;
        }
        public Builder setRoadClearance(int roadClearance)
        {
            if(roadClearance >= SG_Technique.checkRoadClearanceMin && roadClearance <= SG_Technique.checkRoadClearanceMax)
                this.tractorToBuild.roadClearance = roadClearance;
            else
                throw new IllegalArgumentException("The RoadClearance must be higher then 1 and lower then 2720");
            return this;
        }
        //======================================================
        //===================================================
        //========================================================
    }
    //============================================================================

    public int compareTo(Tractor t) {
        return brand.compareTo(t.brand);
        //return 0;
    }

    @Override
    public String toString()
    {
        String str;
        str = "Brand = " + this.brand + " Type_of_engine = " + this.typeOfEngine + " Power = " + this.power +
                " Traction_Class = " + this.tractionClass + " Fuel = " + this.fuel;
       /* if (this.massa > 0 || this.width > 0 || this.height > 0 || this.lenght > 0 || this.roadClearance > 0)
        {
            str += " massa = " + this.massa + " width = " + this.width + " height = " + this.height + " lenght = " + this.lenght +
                    " road_Clearance = " + this.roadClearance;
        }*/
        if(this.massa > 0)
            str += " massa = " + this.massa;
        if(this.width > 0)
            str += " width = " + this.width;
        if(this.height > 0)
            str += " height = " + this.height;
        if(this.lenght > 0)
            str += " lenght = " + this.lenght;
        if(this.roadClearance > 0)
            str += " roadClearance = " + this.roadClearance;

        return str;
    }
    @Override
    public boolean equals(Object obj)
    {
        if(this == obj) return true;
        if((obj == null) || (getClass() != obj.getClass())) return false;
        Tractor other = (Tractor) obj;
        if (brand != other.brand || typeOfEngine != other.typeOfEngine || power != other.power ||
                tractionClass != other.tractionClass || fuel != other.fuel)
            return false;
        return true;
    }
    @Override
    public int hashCode ()
    {
        final int mult = 10;
        int result = 1;
        result = mult * result + this.brand.hashCode();
        result = mult * result + this.typeOfEngine.hashCode();
        result = mult * result + power;
        result = mult * result + tractionClass;
        result = mult * result + fuel;
        return result;
    }

}
