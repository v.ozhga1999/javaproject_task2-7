import com.google.gson.Gson;
import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import javafx.util.converter.IntegerStringConverter;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Arrays.asList;

public class Main {
    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException, TransformerException, JAXBException {
        SG_Technique nameOne = new SG_Technique.Builder()
                .setMassa(1234)
                .setWidth(123)
                .setHeight(123)
                .setLenght(3400)
                .setRoadClearance(1223)
                .build();
        System.out.println(nameOne.toString());
        Tractor t2 = new Tractor.Builder()
                .setBrand("XTZ")
                .setTypeOfEngine("smd-62")
                .setFuel(238)
                .setPower(180)
                .setMassa(123)
                .build();
        //System.out.println(t2.toString());
        Tractor t1 = new Tractor.Builder()
                .setBrand("T-70")
                .setTypeOfEngine("d-62")
                .setFuel(238)
                .setPower(70)
                .setMassa(4150)
                .build();
        Tractor t3 = new Tractor.Builder()
                .setBrand("DT-74")
                .setTypeOfEngine("smd-18")
                .setFuel(238)
                .setPower(95)
                .setMassa(4770)
                .build();
        Tractor t4 = new Tractor.Builder()
                .setBrand("MTZ-80")
                .setTypeOfEngine("d-243")
                .setFuel(238)
                .setPower(80)
                .setMassa(3580)
                .build();
        Tractor t5 = new Tractor.Builder()
                .setBrand("Zetor")
                .setTypeOfEngine("zts-16245")
                .setFuel(236)
                .setPower(165)
                .setMassa(6450)
                .build();
        System.out.println("Collection===================================");
        List<Tractor> collection = new ArrayList<Tractor>();
        collection.add(t1);
        collection.add(t2);
        collection.add(t3);
        collection.add(t4);
        collection.add(t5);
        System.out.println(collection);

        System.out.println("Sorting===================================");
        Collections.sort(collection);
        System.out.println(collection);

        System.out.println("Out=======================================");
        Iterator<Tractor> iterator = collection.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        //Stream===================================
        System.out.println("Stream====================================");
        //Stream<Tractor> stream = collection.stream();
        //stream.forEach(x -> System.out.println(x));
        Stream.of(asList(t1, t2, t3), asList(t4, t5)).flatMap(x -> x.stream()).forEach(x -> System.out.println(x));
        Tractor valueMin = Stream.of(t1, t2, t3, t4, t5).min(Comparator.comparing(x -> x.getPower())).get();
        System.out.println(valueMin);

        Tractor valueMax = Stream.of(t1, t2, t3, t4, t5).max(Comparator.comparing(x -> x.getPower())).get();
        System.out.println(valueMax);

        System.out.println(Stream.of(t1, t2, t3, t4, t5).limit(3).collect(Collectors.toList()));
        Tractor sm1 = Stream.of(t1, t2, t3, t4, t5).filter(x -> x.equals(t1)).findAny().get();
        System.out.println(sm1);

        System.out.println("XML==============================");
        Tractor des;
        Serializer xmlSerialize = new XMLSerialize() {
            @Override
            public Tractor deserialize() throws IOException, JAXBException {
                return null;
            }
        };
        try {
            xmlSerialize.serialize(t1);
            des = xmlSerialize.deserialize();
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        System.out.println("JSON===================================");
        Tractor dejson = null;
        Serializer jsonSerialize = new JsonSerialize();
            jsonSerialize.serialize(t3);
            dejson = jsonSerialize.deserialize();
        System.out.println(dejson.toString());

        System.out.println("TXT===================================");
        Tractor deTxt = null;
        Serializer txtSerialize = new TxtSerialize();
            txtSerialize.serialize(t1);
            deTxt = txtSerialize.deserialize();
        System.out.println(deTxt.toString());

        System.out.println("DataBase====================================");
        List<Tractor> collection0 = new ArrayList<>();
        dataBase db = new dataBase();
        ((dataBase) db).writeDataBaseTractor(collection0);
        System.out.println("twoTables=================================");
        List<Tractor> collection1 = new ArrayList<>();
        dataBase db1 = new dataBase();
        ((dataBase) db1).writeDataBaseTractorAndCharacteristics(collection1);
        System.out.println("FuelConsumption===========================");
        List<Tractor> collection2 = new ArrayList<>();
        dataBase db2 = new dataBase();
        ((dataBase) db2).writeDataBaseTractorFuelConsumption(collection2);
    }
}

