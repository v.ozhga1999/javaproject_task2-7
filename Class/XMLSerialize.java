import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class XMLSerialize implements Serializer
{
    @Override
    public void serialize(Tractor tractor) throws IOException, JAXBException {
        File file = new File("myFile.xml");

        JAXBContext context = JAXBContext.newInstance(Tractor.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(tractor, file);
    }
    @Override
    public abstract Tractor deserialize() throws IOException, JAXBException;
    {
        File file = new File("myFile.xml");
        JAXBContext context = null;
        try {
            context = JAXBContext.newInstance(Tractor.class);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        Unmarshaller unmarshaller = null;
        try {
            unmarshaller = context.createUnmarshaller();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        Tractor tractor = null;
        try {
            tractor = (Tractor) unmarshaller.unmarshal(file);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        System.out.println(tractor.toString());
    }
}
